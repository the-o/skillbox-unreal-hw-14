﻿#include <iostream>
#include <string>

int main()
{
    std::string str{ "Hello, World!" };

    std::cout << str << std::endl;
    std::cout << str.length() << std::endl;
    std::cout << str.substr(0, 1) << std::endl;
    std::cout << str.substr(str.length() - 1, 1) << std::endl;

}

